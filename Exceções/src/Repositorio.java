
public class Repositorio {
	
	private Cliente[]listaCliente = new Cliente[1000];
	
	public void inserirCliente(Cliente c) throws ClienteException, ListaCheiaException{
		int pos = -1;
		
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i] == null){
				pos = i;
			}
			//cliente com o mesmo nome
			if (listaCliente[i] != null && listaCliente[i].getNome().equals(c.getNome())){
				ClienteException e = new ClienteException("Cliente com o mesmo nome");
				throw e;
			}
		}
		//Lista cheia
		if (pos == -1){
			throw new ListaCheiaException("Lista cheia");
		} else {
			listaCliente[pos] = c;
		}
		
	}
	
}
