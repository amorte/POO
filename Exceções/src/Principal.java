
public class Principal {
	
	public static void main(String[] args) {
		
		Repositorio rep = new Repositorio();
		
		
		Cliente c = new Cliente();
		c.setEmail("b@b.com");
		c.setNome("Maria");


		try {
			rep.inserirCliente(c);
		} catch (ClienteException clienteComMesmoNome) {
			System.out.println(clienteComMesmoNome.getMessage());
		} catch (ListaCheiaException e) {
			e.printStackTrace();
		}

		Cliente c2 = new Cliente();
		c2.setEmail("b@b.com");
		c2.setNome("Maria");

		try {
			rep.inserirCliente(c2);
		} catch (ClienteException clienteComMesmoNome) {
			System.out.println(clienteComMesmoNome.getMessage());
		} catch (ListaCheiaException e) {
			e.printStackTrace();
		} finally {
			
		}
	}

}
