
public abstract class Gerente extends Funcionario{

	public Gerente(String nome, int idade) {
		super(nome, idade);
	}

    public abstract void fazerAlgo();
}
