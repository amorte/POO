
public class Carro {
	
	private int codigo;
	private String marca;
	private String tipo;
	private int quantidade;
	private static int valor;
	private final int CODIGO_GERAL = 10;
	
	public String getTipo() {
		return tipo;
	}
	public void setTipo(String tipo) {
		this.tipo = tipo;
	}
	public int getQuantidade() {
		return quantidade;
	}
	public void setQuantidade(int quantidade) {
		this.quantidade = quantidade;
	}
	
	public int getCodigo() {
		return codigo;
	}
	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}
	public String getMarca() {
		return marca;
	}
	public void setMarca(String nome) {
		this.marca = nome;
	}
	
	

}
