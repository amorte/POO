import java.util.Scanner;

public class Principal {
	
	private static Scanner scanner;
	private static Repositorio repositorioCliente;
	
	public static void main(String[] args) {
		scanner = new Scanner(System.in);
		repositorioCliente = new Repositorio();
		
	}
	
	public static void cadastrarCliente(){
		Cliente c = new Cliente();
		System.out.println("Digite o email");
		c.setEmail(scanner.next());
		System.out.println("Digite o endereço");
		c.setEndereco(scanner.next());
		System.out.println("Digite o Nome");
		c.setNome(scanner.next());
		System.out.println("Digite o telefone");
		c.setTelefone(scanner.next());
		
		boolean result = repositorioCliente.cadastrarCliente(c);
		
		if (result) {
			System.out.println("Cliente cadastrado com sucesso");
		}else {
			System.out.println("Cliente não cadastrado");
		}
		
	}

}
