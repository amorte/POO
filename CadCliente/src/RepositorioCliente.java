
public class RepositorioCliente {
	private Cliente[] listaClientes = new Cliente[100];
	private static RepositorioCliente instancia;
	
	public static RepositorioCliente getInstancia(){
		
		if (instancia == null) {
			instancia = new RepositorioCliente();
		}
		
		return instancia;
		
	}
	
	private RepositorioCliente(){
		
	}
	
	public boolean inserirCliente(Cliente c) {
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && 
					listaClientes[i].getCodigo() == c.getCodigo()){
				return false;
			} 
		}
		
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] == null){
				listaClientes[i] = c;
				return true;
			}
		}
		
		return false;
	}
	
	public Cliente procurarCliente(int codigo){
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].getCodigo() == codigo) {
				return listaClientes[i];
			}
		}
		
		return null;
	}
	
    public Cliente[] procurarClientePeloEndereco(String endereco) {
    	Cliente[] resultado = null;
    	int contador = 0;
    	
    	for (int i = 0; i < listaClientes.length; i++) {
    		if(listaClientes[i] != null && listaClientes[i].getEndereco().equals(endereco)){
    			contador++;
    		}
			
		}
    	
    	if (contador > 0) {
    		resultado = new Cliente[contador];
    		int aux = 0;
    		
    		for (int i = 0; i < listaClientes.length; i++) {
    			if (listaClientes[i] != null && listaClientes[i].getEndereco().equals(endereco)){
    				resultado[aux] = listaClientes[i];
    				aux++;
    			}
				
			}
    	}
    	
    	return resultado;
    	
    }
	
	public boolean removerCliente(int codigo){
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].getCodigo() == codigo){
				listaClientes[i] = null;
				return true;
			}
		}
		
		return false;
	}
	
	
	

}
