import java.util.ArrayList;
import java.util.List;

public class RepositorioArray implements IRepositorio{
	
	private List<Cliente> clienteArray;
	
	public RepositorioArray() {
		clienteArray = new ArrayList<Cliente>();
	}

	@Override
	public boolean cadastrarCliente(Cliente c) {
		clienteArray.add(c);
		return false;
	}

	@Override
	public Cliente pesquisarPeloCodigo(int codigo) {
		for (Cliente c : clienteArray) {
			if (c.getCodigo() == codigo) {
				return c;
			}
		}
		
		return null;
	}

	@Override
	public boolean removerCliente(int codigo) {
		for (Cliente c : clienteArray) {
			if (c.getCodigo() == codigo) {
				clienteArray.remove(c);
				return true;
			}
		}
		
		return false;
	}

}
