import java.util.Scanner;

public class Principal {
	
	static Scanner s = new Scanner(System.in);
	
	static private IRepositorio repositorio = new RepositorioBancoDeDados();
	
	public static void main(String[] args) {
		
	}
	
	static void cadastrarCliente(){
		Cliente c = new Cliente();
		System.out.println("Digite o código");
		c.setCodigo(s.nextInt());
		System.out.println("Digite o nome");
		c.setNome(s.next());
		System.out.println("Digite o endereço");
		c.setEndereco(s.next());
		
		repositorio.cadastrarCliente(c);
	}

}
