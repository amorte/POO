
public interface IRepositorio {
	
	public boolean cadastrarCliente(Cliente c);
	public Cliente pesquisarPeloCodigo(int codigo);
    public boolean removerCliente (int codigo);
	

}
