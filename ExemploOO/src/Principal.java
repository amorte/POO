
public class Principal {
	
	public static void main(String[] args) {
		//ExemploCarro();
		ExemploPessoa();
		
	}
	
	static void ExemploPessoa(){
		Pessoa p = new Pessoa("1234556");
	}

	private static void ExemploCarro() {
		Carro bmw = new Carro("bmw", "bmw");
		Carro gol = new Carro("gol", "VW");
		
		
		bmw.cor = 1;
		bmw.fabricante = "BMW";
		bmw.placa = "KKK0000";
		bmw.acelerar(30);
		
		
		bmw.acelerar(30);
		
		gol.cor = 2;
		gol.fabricante = "VW";
		gol.placa = "JJJ7777";
		gol.acelerar(10);
		
		System.out.println("Gol = " + gol.velocidade);
		System.out.println("BMW = " + bmw.velocidade);
	}
	
	
}
