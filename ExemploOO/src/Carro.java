
public class Carro {
	
	int cor;
	String placa;
	String fabricante;
	String modelo;
	int velocidade = 0;
	
	Carro(String mod, String fab){
		modelo = mod;
		fabricante = fab;
	}
	
	
	void acelerar(int valor){
		velocidade += valor;
	}
	
	void freiar(int valor){
		if (velocidade <= valor) {
			velocidade -= valor;
		}
	}

}
