import java.util.Scanner;

public class Matrizes {
	
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		
		int matrizA[][] = new int [2][2];
		int matrizB[][] = new int [2][2];
		
		preencheMatriz(matrizA);
		preencheMatriz(matrizB);
		
		somaMatrizes(matrizA, matrizB);
	}
	
	static void preencheMatriz(int mat[][]){
		Scanner scanner = new Scanner(System.in);
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
			    System.out.println("Digite o valor [" + i + "][" + j + "]");
			    mat[i][j] = scanner.nextInt();
			}
		}
	}
	
	static void somaMatrizes(int matA[][], int matB[][]){
		int matrizResultato[][] = new int [2][2];
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
			    matrizResultato[i][j] = matA[i][j] + matB[i][j];
			    System.out.println("[" + i + "][" + j + "]" + matrizResultato[i][j]);
			}
		}
	}

}
