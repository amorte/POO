import java.util.Scanner;

public class Potencia {
	
	static int valor;
	
	public static void main(String[] args) {
		//Inicializa o scanner
		Scanner scanner = new Scanner(System.in);
		
		System.out.println("Digite a base");
		int base = scanner.nextInt();
		
		System.out.println("Digite a potencia");
		int pot = scanner.nextInt();
		
		//int resultado = potencia(base, pot);
		
        System.out.println("Resultado = " + potencia(base, pot));
		
	}
	
	static int potencia(int base, int potencia){

		int result = 1;
		
		for (int i = 0; i < potencia; i++) {
			result *= base;
		}
		
		return result;
	}
}
