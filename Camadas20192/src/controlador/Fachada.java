package controlador;

import excecoes.ClienteException;
import model.Cliente;

public class Fachada implements IControladorCliente{
	private static Fachada instancia;
	private IControladorCliente controladorCliente;
	
	private Fachada(){
		controladorCliente = new ControladorCliente2();
	}
	
	public static Fachada getInstancia(){
		if (instancia == null) {
			instancia = new Fachada();
		}
		return instancia;
	}

	@Override
	public void cadastrarCliente(Cliente c) throws ClienteException {
		controladorCliente.cadastrarCliente(c);
	}

}
