package controlador;

import excecoes.ClienteException;
import model.Cliente;

public interface IControladorCliente {
	
	public void cadastrarCliente(Cliente c) throws ClienteException;

}
