import java.util.Scanner;

public class HelloWorld {
	public static void main(String[] args) {
		double nota1;
		double nota2;
		double media;
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite nota 1");
		nota1 = s.nextDouble();
		System.out.println("Digite nota 2");
		nota2 = s.nextDouble();
		
		media = (nota1 + nota2) / 2;
		
		if (media >= 7) {
			System.out.println("Aprovado");
		} else if (media < 3) {
			System.out.println("Reprovado");
		} else {
			double notaFinal;
			double mediaFinal;
			
			System.out.println("Digite a nota da final");
			notaFinal = s.nextDouble();
			
			mediaFinal = (media + notaFinal) / 2;
			
			if (mediaFinal >= 5) {
				System.out.println("Aprovado na final");
			} else {
				System.out.println("Reprovado na final");
			}
			
		}
		
		
	}
}
