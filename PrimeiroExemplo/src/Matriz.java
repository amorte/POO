import java.util.Scanner;

public class Matriz {
	
	public static void main(String[] args) {
		Scanner s = new Scanner(System.in);
		int matriz1[][] = new int[2][2];
		int matriz2[][] = new int[2][2];
		int matriz3[][] = new int[2][2];
		
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				matriz1[i][j] = s.nextInt();
			}
			
		}
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				matriz2[i][j] = s.nextInt();
			}
			
		}
		
		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < 2; j++) {
				matriz3[i][j] = matriz1[i][j] + matriz2[i][j];
			}
			
		}
	}

}
