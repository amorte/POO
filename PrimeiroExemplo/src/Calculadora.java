import java.util.Scanner;

public class Calculadora {
	
	static int numero;
	
	public static void main(String[] args) {
		double valor1;
		double valor2;
		int opcao;
		
		Scanner s = new Scanner(System.in);
		System.out.println("Digite o valor 1");
		valor1 = s.nextDouble();
		System.out.println("Digite o valor 2");
		valor2 = s.nextDouble();
		
		System.out.println("Digite: \n1-Soma\n2-Subtração\n"
				+ "3-Muiltiplicação\n4-Divisão\n5-Potência\n6-Fatorial");
	    opcao = s.nextInt();
	    
	    switch(opcao){
	    case 1:
	    	    adicao(valor1,valor2);
	    		break;
	    case 2:
	    	    subtracao(valor1,valor2);
	    		break;
	    case 3:
	    		System.out.println("Multiplicação = " + multiplicacao(valor1, valor2));
	    		break;
	    case 4:
	    	    double divisao = divisao(valor1, valor2);
	    	    if (divisao == 0) {
	    	    	System.out.println("divisão incorreta. ");
	    	    } else {
	    	    		System.out.println("Divisao = " + divisao);
	    	    }
	    		break;
	    case 5:
			System.out.println("Digite o valor da base");
			int base = s.nextInt();
			System.out.println("Digite o valor do expoente");
			int expoente = s.nextInt();
	    	System.out.println("Potência = " + potencia(base, expoente));
	    	 break;
	    case 6: 
	    	System.out.println("Digite um valor");
			int valor = s.nextInt();
	    	System.out.println("Fatorial = " + fatorial(valor));
	    	break;
	    	default:
	    		System.out.println("Opção inválida");
	    		break;
	
	    }
		
	}
	
	static void adicao(double numero1, double numero2){
		System.out.println("A soma é = " + 
				(numero1 + numero2));
	}
	
	static void subtracao(double num1, double num2){
		System.out.println("A subtração é = " + (num1-num2));
	}
	
	static double multiplicacao(double num1, double num2){
		return (num1*num2);
	}
	
	static double divisao (double num1, double num2){
		if (num2 == 0) {
			return 0;
		}else {
			return (num1/num2);	
		}
		
	}

	
	static int potencia(int base, int expoente){
		int pot = base;
		int cont = 1;
		
		while (cont < expoente) {
			pot = pot * base;
			cont++;
		}
		
		return pot;
	}
	
	static int fatorial (int num){
		int fat = 1;
		for (int i = num; i > 1; i--) {
            fat = fat * i;
		}
		return fat;
	}

}
