public class RepositorioCliente {

    private Cliente[] clientes;
    private static final int LIST_SIZE = 100;
    private static RepositorioCliente instancia;

    public static RepositorioCliente getInstancia(){
        if (instancia == null) {
            instancia = new RepositorioCliente();
        }

        return instancia;
    }


    private RepositorioCliente(){

        clientes = new Cliente[LIST_SIZE];
    }


    public boolean inserirCliente(Cliente c){
        if (c == null) {
            return false;
        }
        for (int i = 0; i < clientes.length; i++) {
            if(clientes[i] != null &&
                    (clientes[i].getCpf().equals(c.getCpf()) || clientes[i].getCodigo() == c.getCodigo())){
                return false;
            }
        }

        for (int i = 0; i < clientes.length; i++){
            if (clientes[i] == null){
                clientes[i] = c;
                return true;
            }
        }

        return false;
    }

    public boolean removerCliente(int codigo){
        for (int i = 0; i < clientes.length; i++) {
            if(clientes[i] != null && clientes[i].getCodigo() == codigo) {
                clientes[i] = null;
                return true;
            }

        }
        return false;
    }

    public int removerClientePeloNome(String nome){
        int result = 0;

        for (int i = 0; i < clientes.length; i++) {
            if(clientes[i] != null && clientes[i].getNome().equals(nome)) {
                clientes[i] = null;
                result++;
            }

        }

        return result;
    }

    public Cliente buscarClientePorCodigo(int codigo){

        for (int i = 0; i < clientes.length ; i++) {
            if(clientes[i] != null && clientes[i].getCodigo() == codigo) {
                return clientes[i];
            }
        }
        return null;
    }

    public Cliente[] buscarClientesPorBairro(String bairro){
        Cliente[] result = null;
        int contador = 0;

        for (int i = 0; i < clientes.length ; i++) {
            if(clientes[i] != null && clientes[i].getEndereco() != null
            && clientes[i].getEndereco().getBairro().equals(bairro)) {
                contador++;
            }
        }

        if (contador > 0) {
            result = new Cliente[contador];

            int aux = 0;

            for (int i = 0; i < clientes.length ; i++) {
                if(clientes[i] != null && clientes[i].getEndereco() != null
                        && clientes[i].getEndereco().getBairro().equals(bairro)) {
                    result[aux] = clientes[i];
                    aux++;
                }
            }
        }

        return result;
    }






}
