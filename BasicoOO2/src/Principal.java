
public class Principal {
	
	public static void main(String[] args) {
	
		Conta c1 = new Conta("1234");
		
		c1.creditar(100);
		c1.valor = 5;
		
		Conta.valor = 6;
		
		
		System.out.println("Saldo = " + c1.getSaldo());
		
		if (c1.debitar(200)){
			System.out.println("Saque efetuado com sucesso");
		} else {
			System.out.println("Saldo insuficiente");
		}
		
		
	}

}
