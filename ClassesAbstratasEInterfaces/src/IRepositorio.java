
public interface IRepositorio {
	
	public void inserirAluno(Aluno aluno);
	public Aluno consultar(String nome);
	public void removerAluno(Aluno aluno);

}
