package controlador;

import exececoes.CarroException;
import modelo.Carro;
import repositorio.IRepositorio;
import repositorio.RepositorioCarro;

public class ControladorCarro implements IControlador {
    private IRepositorio repositorioCarro;

    private static ControladorCarro instancia;

    public static ControladorCarro getInstancia(){
        if (instancia == null) {
            instancia = new ControladorCarro();
        }

        return instancia;
    }

    private ControladorCarro(){
        repositorioCarro = RepositorioCarro.getInstancia();
    }

    @Override
    public void inserCarro(Carro c) throws CarroException {

        if (repositorioCarro.existeCarro(c.getPlaca())){
            throw new CarroException("Placa já existe");
        }
        repositorioCarro.inserCarro(c);

    }

    @Override
    public Carro procurarCarroPelaPlaca(String placa) {
        return null;
    }

    @Override
    public void removerCarro(String placa) throws CarroException {

    }
}
