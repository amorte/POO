package repositorio;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import exececoes.CarroException;
import modelo.Carro;

public class RepositorioPostgres implements IRepositorio {
	private  Connection dbConnection;
	private static RepositorioPostgres instancia;
	private static final String JDBC_STRING = "jdbc:postgresql";
	private static final String HOST = "localhost";
	private static final String PORT = "5432";
	private static final String DB_INSTANCE = "postgres";
	private static final String DB_NAME = "postgres";
	private static final String DB_PASSWORD = "password";
	private static final String CONNECTION_STRING = JDBC_STRING + "://"+HOST+":"+PORT+"/"+ DB_INSTANCE; 
	
	private RepositorioPostgres(){
		
	}
	
	public static RepositorioPostgres getInstancia(){
		if (instancia == null) {
			instancia = new RepositorioPostgres();
		}
		return instancia;
	}
	
	private void createConnection() throws SQLException{
            dbConnection =  DriverManager.getConnection(CONNECTION_STRING, DB_NAME, DB_PASSWORD);
	}
	
	private void closeConnection(){
		if (dbConnection != null) {
			try {
				dbConnection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
	
    public boolean isDatabaseOnline(){
   	 try {
			dbConnection =  DriverManager.getConnection(CONNECTION_STRING, DB_NAME, DB_PASSWORD);
			return dbConnection.isValid(6000);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
   }

	
    @Override
    public void inserCarro(Carro c) throws CarroException {
    	
    	try {
    		createConnection();
    		Statement statement = dbConnection.createStatement();
    		String query = "INSERT into public.carro (modelo,marca,placa) values ('" +  c.getModelo() + "','" + c.getMarca() + "','" +  c.getPlaca()  + "');";
    		statement.execute(query);
    	} catch (SQLException e) {
    		e.printStackTrace();
    		throw new CarroException("Erro ao conectar ao banco");
    	} finally {
    		closeConnection();
    	}
    	

    }

    @Override
    public Carro procurarCarroPelaPlaca(String placa) throws CarroException {
    	Carro c = null;
    	
    	try{ 
    		createConnection();
    		Statement statement = dbConnection.createStatement();
    		String query = "SELECT * FROM public.carro where placa = '" +placa + "';";
    		ResultSet resultSet = statement.executeQuery(query);
    		if (resultSet.next()) {
    			c = new Carro();
    			c.setMarca(resultSet.getString("marca"));
    			c.setModelo(resultSet.getString("modelo"));
    			c.setPlaca(resultSet.getString("placa"));
    			c.setCodigo(Integer.parseInt(resultSet.getString("codigo")));
    		}
    		return c;
    	}catch (SQLException e) {
    		e.printStackTrace();
    		throw new CarroException("Erro ao conectar ao banco");
    	} finally {
    		closeConnection();
    	}
    	
    }

    @Override
    public void removerCarro(String placa) throws CarroException {
    	try{ 
    		createConnection();
    		Statement statement = dbConnection.createStatement();
    		String query = "DELETE FROM public.carro where placa = '" +placa + "';";
    		statement.execute(query);
    	}catch (SQLException e) {
    		e.printStackTrace();
    		throw new CarroException("Erro ao conectar ao banco");
    	} finally {
    		closeConnection();
    	}
    }
    
    @Override
    public void atualizarCarro(Carro c) throws CarroException{
    	try{ 
    		createConnection();
    		Statement statement = dbConnection.createStatement();
    		String query = "update public.carro set modelo = '" + c.getModelo() + "',marca = '" + c.getMarca() + "' where placa = '" + c.getPlaca() + "';";
    		statement.execute(query);
    	}catch (SQLException e) {
    		e.printStackTrace();
    		throw new CarroException("Erro ao conectar ao banco");
    	} finally {
    		closeConnection();
    	}
    }
    
    @Override
    public boolean existeCarro(String placa) throws CarroException{
    	try{ 
    		createConnection();
    		Statement statement = dbConnection.createStatement();
    		String query = "SELECT * FROM public.carro where placa = '" +placa + "';";
    		ResultSet resultSet = statement.executeQuery(query);
    		if (resultSet.next()) {
    			return true;
    		}
    		
    	}catch (SQLException e) {
    		e.printStackTrace();
    		throw new CarroException("Erro ao conectar ao banco");
    	} finally {
    		closeConnection();
    	}
        return false;
    }
}
