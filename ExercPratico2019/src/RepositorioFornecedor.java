
public class RepositorioFornecedor {
	
	private Fornecedor[] listaFornecedores = new Fornecedor[100];

	public boolean inserirFornecedor (Fornecedor f){
		for (int i = 0; i < listaFornecedores.length; i++) {
			if (listaFornecedores[i] != null && listaFornecedores[i].getCnpj().equals(f.getCnpj())){
				return false;
			}
		}
		
		for (int i = 0; i < listaFornecedores.length; i++) {
			if (listaFornecedores[i] == null) {
				listaFornecedores[i] = f;
				return true;
			}
		}
		
		return false;
	}
	
	public boolean removerFornecedorPeloCNPJ(String cnpj){
		for (int i = 0; i < listaFornecedores.length; i++) {
			if (listaFornecedores[i] != null && listaFornecedores[i].getCnpj().equals(cnpj)){
				listaFornecedores[i] = null;
				return true;
			}
		}
		return false;
	}
	
	public Fornecedor pesquisarFornecedorPeloTipoDeServico(String tipoDeServico){
		for (int i = 0; i < listaFornecedores.length; i++) {
			if (listaFornecedores[i] != null && listaFornecedores[i].getTipoServico().equals(tipoDeServico)) {
				return listaFornecedores[i];
			}
		}
		
		return null;
	}
	
	public Fornecedor[] pesquisarFornecedorPeloCEP(String CEP){
		Fornecedor[] fornecedores = null;
		int contador = 0;
		
		for (int i = 0; i < listaFornecedores.length; i++) {
			if (listaFornecedores[i] != null && listaFornecedores[i].getEndereco() != null && listaFornecedores[i].getEndereco().getCep().equals(CEP)){
				contador++;
			}
		}
		
		if (contador > 0) {
			fornecedores = new Fornecedor[contador];
			int aux = 0;
			
			for (int i = 0; i < listaFornecedores.length; i++) {
				if (listaFornecedores[i] != null && listaFornecedores[i].getEndereco() != null && listaFornecedores[i].getEndereco().getCep().equals(CEP)){
					fornecedores[aux] = listaFornecedores[i];
					aux++;
				}
			}
		}
		
		return fornecedores;
	}

}
