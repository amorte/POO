
public class RepositorioCliente {
	
	private Cliente [] listaClientes = new Cliente[100];
	
	public boolean inserirCliente(Cliente c){
		//Procura clientes com o mesmo CPF na lista
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && 
					listaClientes[i].getCpf().equals(c.getCpf())){
				return false;
			}
		}
		
		//Procura um espaço vazio
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] == null){
				listaClientes[i] = c;
				return true;
			}
		}
		
		//LISTA CHEIA
		return false;
		
	}
	
	public boolean removerClientePeloCodigo(int codigo){
		//Varre toda a lista
		for (int i = 0; i < listaClientes.length; i++) {
			//se existe o cliente com o código igual, remove.
			if (listaClientes[i] != null && 
					listaClientes[i].getCodigo() == codigo){
				listaClientes[i] = null;
				return true;
			}
		}
		
		return false;
	}
	
	public int removerClientePeloNome(String nome){
		int quantidade = 0;
		
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && 
					listaClientes[i].getNome().equals(nome)){
				listaClientes[i] = null;
				quantidade++;
			}
		}
		
		return quantidade;
	}
	
	//Procura na lista um objeto cliente com o código igual ao do parâmetro
	public Cliente pesquisarClientePeloCodigo(int codigo) {
		
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && 
					listaClientes[i].getCodigo() == codigo) {
				return listaClientes[i];
			}
		}
		
		return null;
	}
	
	//Procura na lista todos os clientes com bairro igual ao do parâmetro. Nesse caso podemos ter 0 ou N clientes, por isso, o retorno do método é um array.
	public Cliente[] pesquisarClientesPeloBairro(String bairro){
		Cliente[] clientes = null;
		int contador = 0;
		
		//varre a lista contando por clientes com o mesmo bairro. Conta 
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].getEndereco() != null && listaClientes[i].getEndereco().getBairro().equals(bairro)) {
				contador++;
			}
		}
		
		//se contador>0, pelo menos um cliente foi encontrado na lista
		if (contador > 0) {
			
			//instancia o array que será retornado
			clientes = new Cliente[contador];
			int auxiliar = 0;
			
			for (int i = 0; i < listaClientes.length; i++) {
				if (listaClientes[i] != null && listaClientes[i].getEndereco() != null &&  listaClientes[i].getEndereco().getBairro().equals(bairro)) {
					clientes[auxiliar] = listaClientes[i];
					auxiliar++;
				}
			}
		}
		
		return clientes;
	}

}
