
public class Carro {
	String marca;
	String modelo;
	double velocidadeMaxima;
	double velocidadeAtual;
	
	Carro(double velocidadeMaxima){
		this.velocidadeMaxima = velocidadeMaxima;
		velocidadeAtual = 0;
	}
	
	void acelerar(double valor){
		if (velocidadeAtual + valor <= velocidadeMaxima){
			velocidadeAtual += valor;
		} else {
			velocidadeAtual = velocidadeMaxima;
		}
	}
	
	void freiar(){
		if (velocidadeAtual -1 >= 0){
			velocidadeAtual--;
		} else {
			velocidadeAtual = 0;
		}
		
	}

}
