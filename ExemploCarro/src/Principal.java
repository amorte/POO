import java.util.Scanner;

public class Principal {
	
	private static Scanner s = new Scanner(System.in);
	private RepositorioAluno repositorio = new RepositorioAluno();
	
	public static void main(String[] args) {
		Principal p = new Principal();
		int opcao = 0;
		
		do {
			
			System.out.println("Escolha uma opção");
			System.out.println("1 - Cadastrar aluno\n2 - Remover Aluno\n3 - Pesquisar aluno pelo login\n4 - Pesquisar alunos pelo nome\n5 - Alterar Senha\n6 - Sair");
			opcao = s.nextInt();
			
			switch (opcao){
			case 1: 
				p.cadastrarAluno();
				break;
			case 2: 
				p.removerAluno();
				break;
			case 3: 
				p.pesquisarAlunoPeloLogin();
				break;
			case 4:
				p.pesquisarAlunosPeloNome();
				break;
			case 5: 
				p.alterarSenha();
				break;
			default:
				System.out.println("Obrigado por utilizar o sistema");
				opcao = 0;
			}
			
		}while (opcao != 0);
		

	}

	private void cadastrarAluno(){
		repositorio = new RepositorioAluno();
		Aluno a = new Aluno();
		System.out.println("Digite o nome");
		a.setNome(s.next());
		System.out.println("Digite o login");
		a.setLogin(s.next());
		
		do {
			System.out.println("Digite a senha");
		} while (!a.setSenha(s.next()));
		
	
		if (repositorio.cadastrarAluno(a) == true){
			System.out.println("Aluno " + a.getNome() + " cadastrado com sucesso");
		} else {
			System.out.println("Lista cheia");
		}

	}
	
	private void removerAluno(){
		String login;
		System.out.println("Digite o ligin a ser removido");
		login = s.next();
		
		if (repositorio.removerAluno(login)) {
			System.out.println("Aluno removido");
		} else {
			System.out.println("Login não encontrado");
		}
	}
	
	private void pesquisarAlunoPeloLogin(){
		String login;
		System.out.println("Digite o login a ser pesquisado");
		login = s.next();
		
		Aluno aluno = repositorio.pesquisarAlunoPeloLogin(login);
		
		if (aluno != null) {
			System.out.println("Aluno " + aluno.getNome() + " encontrado");
		} else {
			System.out.println("Não existem alunos com o login procurado");
		}
	}
	
	private void alterarSenha() {
		
	}

	private void pesquisarAlunosPeloNome() {
		
	}

}
