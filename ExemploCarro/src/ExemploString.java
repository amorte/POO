
public class ExemploString {
	
	public static void main(String[] args) {
		String s1 = "George";
		String s2 = "George";
		
		if (s1 == s2) {
			System.out.println("São iguais");
		} else {
			System.out.println("Não são iguais");
		}
		
		s1 += " Bush";
		s2 += " Bush";
		
		System.out.print("Sem equals : ");
		if (s1 == s2) {
			
			System.out.println("São iguais");
		} else {
			System.out.println("Não são iguais");
		}
		
		System.out.print("Com equals : ");
		if (s1.equals(s2)) {
			System.out.println("São iguais");
		} else {
			System.out.println("Não são iguais");
		}
		
		System.out.println("S1 = " + s1);
		s1 = s1.toUpperCase();
		System.out.println("S1 upper = " + s1);
		System.out.println("Tamanho = " + s1.length());
		
	}

}
