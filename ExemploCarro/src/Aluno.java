
public class Aluno {
	private String nome;
	private String login;
	private String senha;
	
	public void setNome(String nome){
		this.nome = nome;
	}
	
	public String getNome(){
		return this.nome;
	}
	
	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public boolean setSenha(String senha){
		if (senha.length() >= 8){
			this.senha = senha;
			return true;
		}
		return false;
	}
	
	public String getSenha(){
		return this.senha;
	}

}
