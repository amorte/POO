package controller;

import excecoes.ClienteException;
import model.Cliente;
import repositorio.IRepositorio;
import repositorio.RepositorioClienteArray;

public class ControladorCliente implements IControladorCliente{
	
	private IRepositorio repositorioCliente;
	private static ControladorCliente instancia;
	
	protected static ControladorCliente getInstancia(){
		if (instancia == null) {
			instancia = new ControladorCliente();
		}
		return instancia;
	}
	
	private  ControladorCliente() {
		repositorioCliente = RepositorioClienteArray.getInstancia();
	}

	@Override
	public void cadastrarCliente(Cliente c) throws ClienteException{
		repositorioCliente = RepositorioClienteArray.getInstancia();
		
		if (repositorioCliente.existeCliente(c.getCodigo())) {
			throw new ClienteException("Cliente com código já existente");
		} else {
			repositorioCliente.cadastrarCliente(c);
		}
		
	}
	

}
