package controller;

import excecoes.ClienteException;
import model.Cliente;

public class Fachada implements IControladorCliente, IControladorFornecedor {
	
	private IControladorCliente controladorCliente;
	private static Fachada instancia;
	
	public static Fachada getInstancia(){
		if (instancia == null) {
			instancia = new Fachada();
		}
		
		return instancia;
	}
	
	private Fachada(){
		controladorCliente = ControladorCliente.getInstancia();
	}

	@Override
	public void cadastrarFornecedor() {
		
		
	}

	@Override
	public void cadastrarCliente(Cliente c) throws ClienteException{
		controladorCliente.cadastrarCliente(c);

	}

}
