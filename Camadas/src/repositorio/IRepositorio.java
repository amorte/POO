package repositorio;

import excecoes.ClienteException;
import model.Cliente;

public interface IRepositorio {
	public void cadastrarCliente (Cliente c) throws ClienteException;
	public boolean existeCliente(int codigo);
}
