package repositorio;

import java.util.ArrayList;
import java.util.List;

import excecoes.ClienteException;
import model.Cliente;

public class RepositorioClienteArray implements IRepositorio{
	
	private List<Cliente> listaCliente;
	private static RepositorioClienteArray instancia;
	
	public static RepositorioClienteArray getInstancia() {
		if (instancia == null) {
			instancia = new RepositorioClienteArray();
		}
		return instancia;
	}
	
	private  RepositorioClienteArray() {
		listaCliente = new ArrayList<>();	
	}

	@Override
	public void cadastrarCliente(Cliente c) throws ClienteException{
		listaCliente.add(c);
	}

	@Override
	public boolean existeCliente(int codigo) {
		for (Cliente c: listaCliente) {
			if (c.getCodigo() == codigo){
				return true;
			}
		}
		return false;
	}
	
	
	
	

}
