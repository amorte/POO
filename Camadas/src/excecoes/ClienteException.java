package excecoes;

public class ClienteException extends Exception{
	String mensagem;
	
	public ClienteException(String mensagem){
		super(mensagem);
	}
	
	public ClienteException(int codigo){
		switch (codigo) {
		case 1: 
		    mensagem = "mensagem 1";
			break;
		case 2: 
			mensagem = "mensagem 2";
			break;
		}
	}
	
	@Override
	public String getMessage() {
		return mensagem;
	}

}
