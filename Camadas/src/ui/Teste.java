package ui;

public class Teste {
	
	//2 - Cria um atributo do tipo da classe e que independa de instancia
	private static Teste instancia;
    
	//3 - Método para retornar a instancia
	public static Teste getInstancia(){
		// 4- checa se a instancia ja existe e retorna
		if (instancia == null) {
			instancia = new Teste();
		} 
		return instancia;
	}
	
	//1 - protege a criação
	private Teste(){
	}

}
