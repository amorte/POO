
public class Main {
	
	public static void main(String [] args) {
		//exemploConta();
		//exemploCarro();

		Conta c = new Conta(1,1,100);

	}	 
	
	static void exemploCarro(){
		Carro carro1 = new Carro();
		carro1.marca = "vw";
		carro1.modelo = "Gol";
		
		carro1.placa = "KKK-1222";
		carro1.velocidade = 100;
		
		carro1.acelerar(100);
		carro1.freiar(20);
	}
	
	//Exemplos de como criar e acessar os objetos do tipo ContaBancaria
	static void exemploConta(){
		ContaBancaria conta1 = new ContaBancaria();
		conta1.agencia = 123;
		conta1.numero = 123;
		conta1.saldo = 2000;
		conta1.creditar(1000);
		
		ContaBancaria conta2 = new ContaBancaria();
		conta2.agencia = 234;
		conta2.numero = 234;
		conta2.saldo = 1;
		
		ContaBancaria conta3 = new ContaBancaria(345,567);
		conta3.saldo = 100;
		
		if (!conta2.debitar(2)) {
			System.out.println("Valor nao debitado");
		}
		
		System.out.println("Conta 1 = " + conta1.agencia + " numero= " 
		+ conta1.numero + " saldo = " + conta1.saldo);
		
		System.out.println("Conta 2 = " + conta2.agencia + " numero= " 
				+ conta2.numero + " saldo = " + conta2.saldo);
				
	}
	

}
