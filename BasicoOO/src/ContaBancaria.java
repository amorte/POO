
public class ContaBancaria {
	int numero;
	double saldo;
	int agencia;
	
	ContaBancaria(int agencia, int numero) {
		this.agencia = agencia;
		this.numero = numero;
	}
	
    ContaBancaria(){
    	
    }
	
	void creditar(double valor){
		saldo += valor;
	}
	
	boolean debitar (double valor){
		if (valor <= saldo){
			saldo -= valor;
			return true;
		}
		return false;
	}

}
