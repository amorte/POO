public class Conta {

    int numero;
    int agencia;
    double saldo;

    void depositar(double valor){
        saldo += valor;
    }

    Conta(int numero, int agencia, int saldo){
        this.numero = numero;
        this.agencia = agencia;
        this.saldo = saldo;
    }

}
