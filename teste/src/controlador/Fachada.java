package controlador;

import java.util.List;

import exception.ClienteException;
import modelo.Cliente;

public class Fachada implements IContraladorCliente {
	
	private static Fachada instancia;
	private IContraladorCliente controladorCliente;
	
	public static Fachada getInstancia(){
		if (instancia == null) {
			instancia = new Fachada();
		}
		return instancia;
	}
	
	private Fachada(){
		controladorCliente = ControladorCliente.getInstancia();
	}

	@Override
	public void cadastrarCliente(Cliente c) throws ClienteException {
		controladorCliente.cadastrarCliente(c);
	}

	@Override
	public void removerCliente(int codigo) throws ClienteException {
		controladorCliente.removerCliente(codigo);
	}

	@Override
	public Cliente consultarCliente(int codigo) throws ClienteException {
		return consultarCliente(codigo);
	}

	@Override
	public List<Cliente> listarClientes() throws ClienteException {
		return listarClientes();
	}

}
