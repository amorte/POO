package controlador;

import java.util.List;

import exception.ClienteException;
import modelo.Cliente;
import repositorio.IRepositorioCliente;
import repositorio.RepositorioCliente;

public class ControladorCliente implements IContraladorCliente{
	private static ControladorCliente instancia;
	private IRepositorioCliente repositorio;
	
	public static ControladorCliente getInstancia(){
		if (instancia == null) {
			instancia = new ControladorCliente();
		}
		return instancia;
	}
	
	private  ControladorCliente() {
		repositorio = RepositorioCliente.getInstancia();
	}

	@Override
	public void cadastrarCliente(Cliente c) throws ClienteException {
		if (c == null) {
			throw new ClienteException("Objeto inválido");
		} else if (c.getNome().length() < 8) {
			throw new ClienteException("Campo nome com menos de 8 caracteres");
		} else if (repositorio.consultarCliente(c.getCodigo()) != null) {
			throw new ClienteException("Cliente já cadastrado");
		} else {
			repositorio.cadastrarCliente(c);
		}
	}

	@Override
	public void removerCliente(int codigo) throws ClienteException {
		repositorio.removerCliente(codigo);
	}

	@Override
	public Cliente consultarCliente(int codigo) throws ClienteException {
		return null;
	}

	@Override
	public List<Cliente> listarClientes() throws ClienteException {
		return null;
	}

}
