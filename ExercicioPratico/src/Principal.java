import java.util.Scanner;


public class Principal {
	
	private static Scanner scanner;
	private static RepositorioCliente repositorioCliente;
	private static RepositorioFornecedor repositorioFornecedor;

	public static void main(String[] args) {
		
		scanner = new Scanner(System.in);
		//Garanta que essa é a única instancia
		repositorioCliente = RepositorioCliente.getInstancia();
		repositorioFornecedor = RepositorioFornecedor.getInstancia();
	}
	
	private static void removerCliente(){
		repositorioCliente = RepositorioCliente.getInstancia();
	}
	
	private static void inserirCliente(){
		Cliente c = new Cliente();
		System.out.println("Digite o código");
		c.setCodigo(scanner.nextInt());
		System.out.println("Digite o nome");
		c.setNome(scanner.next());
		System.out.println("Digite o email");
		c.setEmail(scanner.next());
		System.out.println("Digite o cpf");
		c.setCpf(scanner.next());
		System.out.println("Digite a data de nascimento");
		c.setDataNascimento(scanner.next());
		
		
		//Cria o objeto endereço e cadastra os valores
		Endereco end = new Endereco();
		System.out.println("Endereço - Digite a rua");
		end.setRua(scanner.next());
		System.out.println("Endereço - Digite o bairro");
		end.setBairro(scanner.next());
		System.out.println("Endereço - Digite o numero");
		end.setNumero(scanner.nextInt());
		System.out.println("Endereço - Digite o complemento");
		end.setComplemento(scanner.next());
		System.out.println("Endereço - Digite o cep");
		end.setCep(scanner.next());
		
		//Seta o endereço criado no objeto cliente
		c.setEndereco(end);
		
		repositorioCliente.inserirCliente(c);
	}
}
