
public class RepositorioFornecedor {
	
	private Fornecedor[] listaFornecedores;
	private static RepositorioFornecedor instancia;
	
	private RepositorioFornecedor() {
		listaFornecedores = new Fornecedor[1000];
	}
	
	public static RepositorioFornecedor getInstancia(){
		if (instancia == null) {
			instancia = new RepositorioFornecedor();
		}
		return instancia;
	}
	
	public boolean inserirFornecedor(Fornecedor fornecedor){
		for (int i = 0; i < listaFornecedores.length; i++) {
			if (listaFornecedores[i] != null && 
					listaFornecedores[i].getCnpj().equals(fornecedor.getCnpj())){
				return false;
			}
		}
		
		for (int i = 0; i < listaFornecedores.length; i++) {
			if (listaFornecedores[i] == null){
				listaFornecedores[i] = fornecedor;
				return true;
			}
		}
		
		return false;
	}
	
	public boolean removerFornecedor(String cnpj){
		for (int i = 0; i < listaFornecedores.length; i++) {
			if (listaFornecedores[i] != null && 
					listaFornecedores[i].getCnpj().equals(cnpj)){
				listaFornecedores[i] =  null;
				return true;
			}
		}
		return false;
	}
	
	public Fornecedor[] buscarFornecedoresPorTipodeServico(int tipoServico){
		Fornecedor[] fornecedores = null;
		int cont = 0;
		
		for (int i = 0; i < listaFornecedores.length; i++) {
			if (listaFornecedores[i] != null && listaFornecedores[i].getTipoDeServico() == tipoServico){
				cont++;
			}
		}
		
		if (cont > 0){
			int pos = 0;
			fornecedores = new Fornecedor[cont];
			for (int i = 0; i < listaFornecedores.length; i++) {
				if (listaFornecedores[i] != null && listaFornecedores[i].getTipoDeServico() == tipoServico){
					fornecedores[pos] = listaFornecedores[i];
					pos++;
				}
			}
		}
		
		return fornecedores;
	}
	
	public Fornecedor[] buscarFornecedoresPorCep(String cep){
		Fornecedor[] fornecedores = null;
		int cont = 0;
		
		for (int i = 0; i < listaFornecedores.length; i++) {
			if (listaFornecedores[i] != null && 
					listaFornecedores[i].getEndereco().getCep().equals(cep)){
				cont++;
			}
		}
		
		if (cont > 0){
			int pos = 0;
			fornecedores = new Fornecedor[cont];
			for (int i = 0; i < listaFornecedores.length; i++) {
				if (listaFornecedores[i] != null && 
						listaFornecedores[i].getEndereco().getCep().equals(cep)){
					fornecedores[pos] = listaFornecedores[i];
					pos++;
				}
			}
		}
		
		return fornecedores;
	}

}
