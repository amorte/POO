
public class Fornecedor {
	
	private String cnpj;
	private String nome;
	private String nomeFantasia;
	private String email;
	private int tipoDeServico;
	private Endereco endereco;
	
	
	public String getCnpj() {
		return cnpj;
	}
	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getNomeFantasia() {
		return nomeFantasia;
	}
	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public int getTipoDeServico() {
		return tipoDeServico;
	}
	public void setTipoDeServico(int tipoDeServico) {
		this.tipoDeServico = tipoDeServico;
	}
	public Endereco getEndereco() {
		return endereco;
	}
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}
	
	

}
