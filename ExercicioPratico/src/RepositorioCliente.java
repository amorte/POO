
public class RepositorioCliente {
	
	private Cliente[] listaClientes;
	
	private static RepositorioCliente instancia;
	
	public static RepositorioCliente getInstancia(){
		if (instancia == null) {
			instancia = new RepositorioCliente();
		}
		return instancia;
	}
	
	private RepositorioCliente(){
		listaClientes = new Cliente[1000];
	}
	
	
	public boolean inserirCliente(Cliente c){
		
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].getCpf().equals(c.getCpf()) || 
					listaClientes[i].getCodigo() == (c.getCodigo())) {
					
				return false;
			}
		}
		
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] == null) {
				listaClientes[i] = c;
				return true;
			}
		}
		
		return false;
		
	}
	
	public boolean inserirClientePerformatico(Cliente c){
		int position = -1;
		
		for (int i = 0; i < listaClientes.length; i++) {
			if (position == -1 && listaClientes[i] == null) {
				position = i;
			}
			
			if (listaClientes[i] != null && (listaClientes[i].getCpf().equals(c.getCpf()) || 
					listaClientes[i].getCodigo() == (c.getCodigo()))) {
					
				return false;
			}
		}
		
		
		if (position != -1){
			listaClientes[position] = c;
			return true;
		}
		
		return false;
		
	}
	
	public boolean removerCliente(int codigo) {
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && 
					listaClientes[i].getCodigo() == codigo){
				listaClientes[i] = null;
				return true;
			}
		}
		return false;
	}
	
	public int removerClientePorNome(String nome) {
		int count = 0;
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].getNome().
					equals(nome)){
				listaClientes[i] = null;
				count++;
			}
		}
		return count;
	}
	
	public Cliente buscarClientePorCodigo(int codigo){
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].
					getCodigo() == codigo){
				return listaClientes[i];
			}
		}
		
		return null;
	}
	
	public Cliente[] buscarClientePorBairro(String bairro){
		Cliente[] clientes = null;
		int cont = 0;
		
		for (int i = 0; i < listaClientes.length; i++) {
			if (listaClientes[i] != null && listaClientes[i].getEndereco().
					
					getBairro().equals(bairro)){
				cont++;
			}
		}
		
		if (cont > 0) {
			clientes = new Cliente[cont];
			int position = 0;
			for (int i = 0; i < listaClientes.length; i++) {
				if (listaClientes[i] != null && listaClientes[i].getEndereco().
						getBairro().equals(bairro)){
					clientes[position] = listaClientes[i];
					position++;
				}
			}
		}
		
		return null;
	}

}
