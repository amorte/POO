package repositorio;

import exececoes.CarroException;
import modelo.Carro;

public interface IRepositorio{
    public void inserCarro(Carro c) throws CarroException;
    public Carro procurarCarroPelaPlaca(String placa) throws CarroException;
    public void removerCarro(String placa) throws CarroException;
    public boolean existeCarro(String placa) throws CarroException;
    public void atualizarCarro(Carro c) throws CarroException;
    public boolean isDatabaseOnline();

}
