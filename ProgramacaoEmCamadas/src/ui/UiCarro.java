package ui;

import controlador.Fachada;
import exececoes.CarroException;
import modelo.Carro;

import java.util.Scanner;

public class UiCarro {

    static Scanner s = new Scanner(System.in);

    public void showMenu(){

        int opcao;

        s = new Scanner(System.in);


        System.out.println("Escolha uma opção \n1 - Inserir carro \n2 - Remover \n3 - Procurar carro \n4 - Atualizar carro \n5 -Voltar");
        opcao = s.nextInt();

        switch (opcao){
            case 1:
                inserirCarro();
                break;
            case 2: 
            	removerCarro();
            	break;
            case 3: 
            	procurarCarro();
            	break;
            case 4: 
            	atualizarCarro();
            	break;
            default:
            	System.out.println("Opção inválida");
            	break;
        }

    }

    private void atualizarCarro() {
    	Carro c = new Carro();
        System.out.println("Digite a marca");
        c.setMarca(s.next());
        System.out.println("Digite o modelo");
        c.setModelo(s.next());
        System.out.println("Digite a placa");
        c.setPlaca(s.next());

        try {
            Fachada.getInstancia().atualizarCarro(c);
            System.out.println("Carro atualizado com sucesso");
        }catch (CarroException e){
        	e.printStackTrace();
            System.out.println(e.getMessage());
        }
	}

	private void removerCarro() {
		System.out.println("Digite a placa do carro a ser removido");
		String placa = s.next();
		
		try {
			Fachada.getInstancia().removerCarro(placa);
			System.out.println("Carro removido com sucesso");
		} catch (CarroException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	private void procurarCarro() {
		System.out.println("Digite a placa");
		String placa = s.next();
		
		try {
			Carro c = Fachada.getInstancia().procurarCarroPelaPlaca(placa);
			if (c != null) {
				System.out.println(c.toString());
			} else {
				System.out.println("Carro não encontrado");
			}
		} catch (CarroException e) {
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	private void inserirCarro(){
        Carro c = new Carro();
        System.out.println("Digite a marca");
        c.setMarca(s.next());
        System.out.println("Digite o modelo");
        c.setModelo(s.next());
        System.out.println("Digite a placa");
        c.setPlaca(s.next());

        try {
            Fachada.getInstancia().inserCarro(c);
            System.out.println("Carro inserido com sucesso");
        }catch (CarroException e){
        	e.printStackTrace();
            System.out.println(e.getMessage());
        }



    }
}
