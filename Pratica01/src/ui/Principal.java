package ui;

import java.util.Scanner;

import controlador.Fachada;
import exception.ClienteException;
import modelo.Cliente;

public class Principal {
	static Scanner s = new Scanner(System.in);
	
	public static void main(String[] args) {
		
	}
	
	public static void cadastrarCliente(){
		Cliente c = new Cliente();
		System.out.println("Digite o nome");
		c.setNome(s.next());
		System.out.println("Digite o nome do usuário");
		c.setNomeUsuario(s.next());
		System.out.println("Digite o código");
		c.setCodigo(s.nextInt());
		
		try {
			Fachada.getInstancia().cadastrarCliente(c);
		} catch (ClienteException e) {
			System.out.println(e.getMessage());
		}
	}

}
