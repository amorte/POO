package repositorio;

import java.util.List;

import exception.ClienteException;
import modelo.Cliente;

public interface IRepositorioCliente {
	public void cadastrarCliente(Cliente c) throws ClienteException;
	public void removerCliente(int codigo) throws ClienteException;
	public Cliente consultarCliente(int codigo) throws ClienteException;
	public List<Cliente> listarClientes() throws ClienteException;

}
