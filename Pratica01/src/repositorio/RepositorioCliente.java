package repositorio;

import java.util.ArrayList;
import java.util.List;

import exception.ClienteException;
import modelo.Cliente;

public class RepositorioCliente implements IRepositorioCliente {
	private List<Cliente> listaClientes;
	private static RepositorioCliente instancia;
	
	private  RepositorioCliente() {
		listaClientes = new ArrayList<Cliente>();
	}
	
	public static RepositorioCliente getInstancia(){
		if (instancia == null) {
			instancia = new RepositorioCliente();
		}
		return instancia;
	}

	@Override
	public void cadastrarCliente(Cliente c) throws ClienteException {
		listaClientes.add(c);
		
	}

	@Override
	public void removerCliente(int codigo) throws ClienteException {
		for (Cliente c: listaClientes) {
			if (c.getCodigo() == codigo){
				listaClientes.remove(c);
				return;
			}
		}
		throw new ClienteException("Usuário não encontrado");
		
	}

	@Override
	public Cliente consultarCliente(int codigo) throws ClienteException {
		for (Cliente c : listaClientes){
			if (c.getCodigo() == codigo) {
				return c;
			}
		}
		return null;
	}

	@Override
	public List<Cliente> listarClientes() throws ClienteException {
		return null;
	}

}
