
public class Principal {
	
	public static void main(String[] args) {
		Cliente cliente = new Cliente();
		cliente.setIdade(19);
		cliente.setNome("joao");
		cliente.setCpf("123");
		
		RepositorioCliente rep = RepositorioCliente.getInstancia();
		
		rep.inserirCliente(cliente);
	}

}
