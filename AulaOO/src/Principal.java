
public class Principal {
	
	public static void main(String[] args) {
		
		Carro bmw = new Carro();
	    
	    bmw.cor = "Branca";
	    bmw.marca = "BMW";
	    bmw.modelo = "Modelo A";
	    bmw.velocidadeMaxima = 250;
	    bmw.velocidade = 0;
	    
	    Carro fusca = new Carro();
	    
	    fusca.cor = "Azul";
	    fusca.marca = "VW";
	    fusca.modelo = "Fusca";
	    fusca.velocidadeMaxima = 150;
	    fusca.velocidade = 0;
		
		
	}

}
