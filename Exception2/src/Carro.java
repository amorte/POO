
public class Carro {
	
	private String marca;
	private String modelo;
	private double velocidade;
	private double velocidadeMaxima;
	
	public Carro(double velocidadeMaxima) {
		this.velocidadeMaxima = velocidadeMaxima;
		velocidade = 0;
	}
	
	public void acelerar (double aceleracao) throws AceleracaoException, FrearException{
		if ((velocidade + aceleracao) > velocidadeMaxima ) {
			throw new AceleracaoException("Valocidade maior que a máxima");
		}
		
		if (aceleracao > 20) {
			throw new AceleracaoException("Aceleração maior que a permitida");
		}
		
		velocidade+=aceleracao;
	}
	
	public void frear (double reducao) throws FrearException{
		if ((velocidade - reducao) <  0 ) {
			throw new FrearException("Valocidade negativa");
		}
		
		if (reducao > 10) {
			throw new FrearException("Redução maior que a permitida");
		}
		
		velocidade-=reducao;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}
	
	public double getVelocidade(){
		return this.velocidade;
	}

}
