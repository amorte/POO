public class Main {

    public static void main(String[] args) {
        Cliente c = new Cliente();

        IRepositorioCliente repositorioCliente =
                new RepositorioClienteMySQL();

        repositorioCliente.fazerA();

        repositorioCliente.cadastrarCliente(c);
    }
}
