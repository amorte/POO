public interface IRepositorioCliente {

    public void cadastrarCliente(Cliente c);
    public Cliente procurarCliente(int codigo);
    public int removerCliente(Cliente c);
}
