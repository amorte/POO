package repositorio;

import excecao.ClienteException;
import model.Cliente;

public interface IRepositorioCliente {
	
	public void inserirCliente(Cliente c) throws ClienteException;
	public void removerCliente(int codigo) throws ClienteException;
	public Cliente procurarCliente(int codigo) throws ClienteException;

}
