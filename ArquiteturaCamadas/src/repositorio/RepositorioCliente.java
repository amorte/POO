package repositorio;

import java.util.ArrayList;
import java.util.List;

import excecao.ClienteException;
import model.Cliente;

public class RepositorioCliente implements IRepositorioCliente{
	
	private List<Cliente> listaCliente;
	private static RepositorioCliente instancia;
	
	private  RepositorioCliente() {
		listaCliente = new ArrayList<Cliente>();
	}
	
	public static RepositorioCliente getInstance(){
		if (instancia == null){
			instancia = new RepositorioCliente();
		}
		return instancia;
	}

	@Override
	public void inserirCliente(Cliente c) throws ClienteException {
		listaCliente.add(c);
	}

	@Override
	public void removerCliente(int codigo) throws ClienteException {
		
	}

	@Override
	public Cliente procurarCliente(int codigo) throws ClienteException {
		return null;
	}

}
