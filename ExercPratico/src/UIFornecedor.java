import java.util.Scanner;

public class UIFornecedor {
	private static Scanner scanner = new Scanner(System.in);
	
	public void showMenu(){
		
		int opcao = 0;
		
		do{
			
			System.out.println("======Cadastro de Fornecedores======");
			System.out.println("Escolha uma opção.\n1 - Inserir fornecedor\n2 - Remover fornecedor pelo cnpj\n"
					+ "3 - Pesquisar fornecedor pelo tipo de serviço\n4 - Pesquisar forncededor pelo CEP\n"
					+ "0 - Voltar ao menu principal");
			
			opcao = scanner.nextInt();
			
			switch(opcao){
				case 1: 
					inserirFornecedor();
					break;
				case 2: 
					removerFornecedorCNPJ();
					break;
				case 3: 
					pesquisarFornecedorTipoServico();
					break;
				case 4: 
					pesquisarFornecedorCEP();
					break;
				case 0:
					break;
				default:
					System.out.println("Opção inválida");
					break;
			}
			
		}while (opcao != 0);
	}

	private void pesquisarFornecedorCEP() {
		
	}

	private void pesquisarFornecedorTipoServico() {
		
	}

	private void removerFornecedorCNPJ() {
		
	}

	private void inserirFornecedor() {
		
	}
	
	private Endereco criarEndereco(){
		System.out.println("Digite a rua");
		String rua = scanner.next();
		System.out.println("Digite o bairro");
		String bairro = scanner.next();
		System.out.println("Digite o número");
		int numero = scanner.nextInt();
		System.out.println("Digite o complemento");
		String complemento = scanner.next();
		System.out.println("Digite o CEP");
		String cep = scanner.next();
		
		
		Endereco e = new Endereco(rua, bairro, numero, complemento, cep);
		return e;
	}


}
