
public class RepositorioCliente {
	
	private static RepositorioCliente instancia;
	private Cliente[] listaCliente;
	
	private RepositorioCliente(){
		listaCliente = new Cliente[100];
	}
	
	public static RepositorioCliente getInstancia(){
		if (instancia == null) {
			instancia = new RepositorioCliente();
		}
		
		return instancia;
	}
	
	public boolean inserirCliente(Cliente c){
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i] != null && listaCliente[i].getCpf()
					.equals(c.getCpf())){
				return false;
			}
		}
		
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i] == null){
				listaCliente[i] = c;
				return true;
			}
		}
		
		return false;
		
	}
	
	public boolean removerClientePeloCPF(String cpf){
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i] != null && listaCliente[i].getCpf().
					equals(cpf)){
				listaCliente[i] = null;
				return true;
			}
		}
		return false;
	}
	
	public int removerClientePeloNome(String nome){
		int cont = 0;
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i] != null && listaCliente[i].getNome().
					equals(nome)){
				listaCliente[i] = null;
				cont++;
			}
		}
		return cont;
	}
	
	public Cliente pesquisarClientePeloCPF(String cpf){
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i] != null && listaCliente[i].getCpf().
					equals(cpf)){
				return listaCliente[i];
			}
		}
		return null;
	}
	
	public Cliente[] pesquisarClientePeloBairro(String bairro){
		Cliente[] resultado = null;
		int cont = 0;
		for (int i = 0; i < listaCliente.length; i++) {
			if (listaCliente[i] != null && listaCliente[i].getEndereco() != null &&
					listaCliente[i].getEndereco().getBairro().
					equals(bairro)){
				cont++;
			}
		}
		
		if (cont > 0) {
			resultado = new Cliente[cont];
			
			int aux = 0;
			for (int i = 0; i < listaCliente.length; i++) {
				if (listaCliente[i] != null && listaCliente[i].getEndereco() != null 
						&&
						listaCliente[i].getEndereco().getBairro().
						equals(bairro)){
					resultado[aux] = listaCliente[i];
					aux++;
				}
			}
		}
		
		return resultado;
	}

}
