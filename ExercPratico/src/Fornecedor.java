
public class Fornecedor {
	private String cnpj;
	private String nome;
	private String nomeFantasia;
	private String dataFundacao;
	private Endereco endereco;
	private String email;
	private String tipoServico;
	
	public Fornecedor(String cnpj, String nome, String nomeFantasia, String dataFundacao, Endereco endereco,
			String email, String tipoServico) {
		this.cnpj = cnpj;
		this.nome = nome;
		this.nomeFantasia = nomeFantasia;
		this.dataFundacao = dataFundacao;
		this.endereco = endereco;
		this.email = email;
		this.tipoServico = tipoServico;
	}
	
	public Fornecedor(){
		
	}

	@Override
	public String toString() {
		return "Fornecedor [cnpj=" + cnpj + ", nome=" + nome + ", nomeFantasia=" + nomeFantasia + ", dataFundacao="
				+ dataFundacao +   ", email=" + email + ", tipoServico=" + tipoServico + "]";
	}

	public String getCnpj() {
		return cnpj;
	}

	public void setCnpj(String cnpj) {
		this.cnpj = cnpj;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getNomeFantasia() {
		return nomeFantasia;
	}

	public void setNomeFantasia(String nomeFantasia) {
		this.nomeFantasia = nomeFantasia;
	}

	public String getDataFundacao() {
		return dataFundacao;
	}

	public void setDataFundacao(String dataFundacao) {
		this.dataFundacao = dataFundacao;
	}

	public Endereco getEndereco() {
		return endereco;
	}

	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTipoServico() {
		return tipoServico;
	}

	public void setTipoServico(String tipoServico) {
		this.tipoServico = tipoServico;
	}
	
	
	

}
