import java.util.Scanner;

public class Principal {
	
	static Scanner scanner = new Scanner(System.in);
	
	public static void main(String[] args) {
		int opcao = 0;
		
		do {
			
			System.out.println("Bem vindo ao sistema de cadastro");
			System.out.println("Escolha uma opção.\n1 - Cadastro de clietes\n2 - Cadastro de fornecedores\n0 - Sair");
			opcao = scanner.nextInt();
			
			switch(opcao) {
			case 1: 
				exibirCadastroCliente();
				break;
			case 2: 
				exibirCadastroFornecedor();
				break;
			case 0:
				System.out.println("Até logo!");
				break;
			default:
				System.out.println("Opção inválida");
			}
			
		} while (opcao != 0);
		
	}
	
	private static void exibirCadastroCliente(){
		UICliente uiCliente = new UICliente();
		uiCliente.showMenu();
		
	}
	
	private static void exibirCadastroFornecedor(){
		UIFornecedor uiFornecedor = new UIFornecedor();
		uiFornecedor.showMenu();
	}

}
