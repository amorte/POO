import java.util.Scanner;

public class Principal {
	
	public static void main(String[] args) {
		Produto p1 = null;
		
		String nome;
		int codigo;
		
		Scanner s = new Scanner(System.in);
		
		System.out.println("Digite o código do produto");
		codigo = s.nextInt();
		System.out.println("Digite o nome do produto");
		nome = s.next();
		
		p1 = new Produto(1, "maria");
		
		System.out.println("Digite a cor do produto");
		p1.cor = s.next();
		System.out.println("Digite a marca do produto");
		p1.marca = s.next();
		
		System.out.println("P1 = " + p1);
		System.out.println("Nome de p1 = " + p1.nome);
		
	}

}
