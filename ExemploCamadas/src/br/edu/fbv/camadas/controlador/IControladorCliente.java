package br.edu.fbv.camadas.controlador;

import br.edu.fbv.camadas.excecoes.ClienteException;
import br.edu.fbv.camadas.model.Cliente;

public interface IControladorCliente {
	/**
	 * 
	 * @param c
	 * @throws ClienteException
	 */
	public void inserirCliente(Cliente c) throws ClienteException;
	public void removerCliente(Cliente c) throws ClienteException;
	public Cliente procurarCliente(String nome);

}
