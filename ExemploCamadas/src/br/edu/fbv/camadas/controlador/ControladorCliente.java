package br.edu.fbv.camadas.controlador;

import br.edu.fbv.camadas.excecoes.ClienteException;
import br.edu.fbv.camadas.model.Cliente;
import br.edu.fbv.camadas.repositorio.IRepositorioCliente;
import br.edu.fbv.camadas.repositorio.RepositorioCliente;
import br.edu.fbv.camadas.repositorio.RepositorioClienteMySql;

public class ControladorCliente implements IControladorCliente{
	
	private static ControladorCliente instancia;
	private IRepositorioCliente repositorioCliente;
	
	public static ControladorCliente getInstance(){
		if (instancia == null){
			instancia = new ControladorCliente();
		}
		return instancia;
	}
	
	private  ControladorCliente() {

		repositorioCliente = new RepositorioClienteMySql();
	}
	
	@Override
	public void inserirCliente(Cliente c) throws ClienteException {
		//insera sua regra aqui
		repositorioCliente.inserirCliente(c);
		
	}

	@Override
	public void removerCliente(Cliente c) throws ClienteException {
		repositorioCliente.removerCliente(c);
	}

	@Override
	public Cliente procurarCliente(String nome) {
		
		return repositorioCliente.procurarCliente(nome);
	}

	public void fazerAlgo(){

	}

}
