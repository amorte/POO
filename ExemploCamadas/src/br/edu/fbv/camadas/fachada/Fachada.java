package br.edu.fbv.camadas.fachada;

import br.edu.fbv.camadas.controlador.ControladorCliente;
import br.edu.fbv.camadas.controlador.IControladorCliente;
import br.edu.fbv.camadas.excecoes.ClienteException;
import br.edu.fbv.camadas.model.Cliente;

public class Fachada implements IControladorCliente{
	
	private static Fachada instancia;
	private IControladorCliente controladorCliente;
	
	/**
	 * 
	 * @return
	 */
	public static Fachada getInstance(){
		if (instancia == null){
			instancia = new Fachada();
		}
		return instancia;
	}
	
	private Fachada(){
		controladorCliente = ControladorCliente.getInstance();
	}

	
	@Override
	/**
	 * 
	 */
	public void inserirCliente(Cliente c) throws ClienteException {
		controladorCliente.inserirCliente(c);
	}

	@Override
	public void removerCliente(Cliente c) throws ClienteException {
		controladorCliente.removerCliente(c);
	}

	@Override
	public Cliente procurarCliente(String nome) {
		return procurarCliente(nome);
	}
	

	
	

}
