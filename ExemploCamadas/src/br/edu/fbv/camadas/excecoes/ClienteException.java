package br.edu.fbv.camadas.excecoes;

public class ClienteException extends Exception{
	
	private String mensagem;
	
	public ClienteException(String msg) {
		mensagem = msg;
	}
	
	@Override
	public String getMessage() {
		return mensagem;
	}

}
