package br.edu.fbv.camadas.ui;

import br.edu.fbv.camadas.controlador.ControladorCliente;
import br.edu.fbv.camadas.controlador.IControladorCliente;
import br.edu.fbv.camadas.excecoes.ClienteException;
import br.edu.fbv.camadas.fachada.Fachada;
import br.edu.fbv.camadas.model.Cliente;

public class UICliente {
	
	public static void main(String[] args) {
		
		///Fachada fachada = Fachada.getInstance();
		IControladorCliente controladorCliente = ControladorCliente.getInstance();
		
		Cliente cliente = new Cliente();
		//digitar dados
		cliente.setCodigo(1);
		cliente.setNome("Maria");
		
		try {
			controladorCliente.inserirCliente(cliente);

		} catch (ClienteException e) {
			e.printStackTrace();
		}
		
	}

}
