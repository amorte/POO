package br.edu.fbv.camadas.repositorio;

import java.util.ArrayList;

import br.edu.fbv.camadas.excecoes.ClienteException;
import br.edu.fbv.camadas.model.Cliente;

public class RepositorioCliente implements IRepositorioCliente{
	
	private ArrayList<Cliente>clientes;
	private static RepositorioCliente instancia;
	
	public static RepositorioCliente getInstance(){
		if (instancia == null) {
			instancia = new RepositorioCliente();
		}
		return instancia;
	}
	
	private RepositorioCliente() {
		clientes = new ArrayList<Cliente>();
	}

	@Override
	public void inserirCliente(Cliente c) throws ClienteException {
		clientes.add(c);
	}

	@Override
	public void removerCliente(Cliente c) throws ClienteException {
		clientes.remove(c);
	}

	@Override
	public Cliente procurarCliente(String nome) {
		for (Cliente c : clientes){
			if (c.getNome().equals(nome)){
				return c;
			}
		}
		return null;
	}
	
	public void init(){
		
	}

}
