public class RepositorioProduto {

	private Produto[] listaProduto = new Produto[100];

	public boolean cadastrarProduto(Produto produto) {

		// Procura um produto com nome ou código igual na lista
		for (int i = 0; i < listaProduto.length; i++) {
			if (listaProduto[i] != null && listaProduto[i].getNome().equals(produto.getNome())
					&& listaProduto[i].getTipo().getCodigo() == produto.getTipo().getCodigo()) {
				int qtdTotal = produto.getQuantidade() + listaProduto[i].getQuantidade();

				if (qtdTotal <= 1000) {
					listaProduto[i].setQuantidade(qtdTotal);
				} else {
					listaProduto[i].setQuantidade(1000);
				}

				return true;
			}
		}

		// procura um espaço na lista
		for (int i = 0; i < listaProduto.length; i++) {
			if (listaProduto[i] == null) {
				listaProduto[i] = produto;
				return true;
			}
		}

		return false;
	}

	public Produto[] consultarProduto(TipoProduto tipo) {
		Produto resultado[] = null;
		int qtd = 0;

		for (int i = 0; i < listaProduto.length; i++) {
			if (listaProduto[i] != null && listaProduto[i].getTipo().getCodigo() == tipo.getCodigo()) {
				qtd++;
			}
		}

		if (qtd > 0) {
			resultado = new Produto[qtd];
			int aux = 0;

			for (int i = 0; i < listaProduto.length; i++) {
				if (listaProduto[i] != null && listaProduto[i].getTipo().getCodigo() == tipo.getCodigo()) {
					resultado[aux] = listaProduto[i];
					aux++;
				}
			}
		}

		return resultado;

	}

	public int removerProduto(TipoProduto tipo, int quantidade) {
		int qtdTotal = 0;

		for (int i = 0; i < listaProduto.length; i++) {

			if (listaProduto[i] != null && listaProduto[i].getTipo().getDescricao().equals(tipo.getDescricao())) {
				if (quantidade > 0) {
					int qtdTemp = listaProduto[i].getQuantidade() - quantidade;

					if (qtdTemp < 0) {
						listaProduto[i].setQuantidade(0);
					} else {
						listaProduto[i].setQuantidade(quantidade);
					}
					qtdTotal += qtdTemp;
				} else {
					listaProduto[i] = null;
					qtdTotal = -1;
				}
			}
		}
		return qtdTotal;
	}
	
	public int removerProdutoEng(TipoProduto tipo){
		int total = 0;
		
		for (int i = 0; i < listaProduto.length; i++) {
			if (listaProduto[i] != null && listaProduto[i].getTipo().getDescricao().equals(tipo.getDescricao())) {
				listaProduto[i] = null;
				total++;
			}
			
		}
		
		return total;
    }


}
