import java.util.Scanner;

public class Questoes15e16 {

    static int[] interseccao(int vetorA[], int vetorB[]){
        int[] resultado = null;
        int tamanho = 0;

        for (int i = 0; i < vetorA.length; i++){
            for (int j = 0; j < vetorB.length; j++){
                if (vetorA[i] == vetorB[j]){
                    tamanho++;
                }
            }
        }

        if (tamanho > 0) {
            resultado = new int [tamanho];
            int position = 0;

            for (int i = 0; i < vetorA.length; i++){
                for (int j = 0; j < vetorB.length; j++){
                    if (vetorA[i] == vetorB[j]){
                        resultado[position] = vetorA[i];
                        position++;
                    }
                }
            }
        }

        return resultado;
    }

    static int[] intercalacao(int vetorA[], int vetorB[]){
        int [] resultado = null;

        resultado = new int [vetorA.length + vetorB.length];

        int pos = 0;
        for (int i = 0; i < vetorA.length; i++) {
            resultado[pos++] = vetorA[i];

            resultado[pos++] = vetorB[i];

        }

        return resultado;
    }

    static void preencheArray(int [] array) {
        Scanner s = new Scanner(System.in);
        for (int i = 0; i < array.length; i++) {
            System.out.println("Digite um valor");
            array[i] = s.nextInt();
        }
    }

    public static void main(String[] args) {
        int vetorA [];
        int vetorB [];
        Scanner s = new Scanner(System.in);
        int n;

        System.out.println("Digite o valor de N");
        n = s.nextInt();

        if (n > 0) {
            vetorA = new int[n];
            vetorB = new int[n];

            preencheArray(vetorA);
            preencheArray(vetorB);

            int [] interec = interseccao(vetorA, vetorB);

            if (interec != null) {
                System.out.println("intersecção");
                for (int i = 0; i < interec.length; i++) {
                    System.out.println(interec[i]);
                }
            } else {
                System.out.println("Não existem elementos na intersecção");
            }

            int [] intercalacao = intercalacao(vetorA, vetorB);

            if (intercalacao != null){
                System.out.println("intercalação");
                for (int i = 0; i < intercalacao.length; i++) {
                    System.out.println(intercalacao[i]);
                }
            } else {
                System.out.println("Não existem elementos na intercalação");
            }


        } else {
            System.out.println("Valor de N inválido");
        }
    }
}
