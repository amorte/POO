import java.util.Scanner;

public class Questao7 {

    public static void main(String[] args) {
        int nota1;
        int nota2;
        Scanner s = new Scanner(System.in);

        for (int i = 1; i <= 30; i++){
            System.out.println("Digite a nota do aluno 1 " + i);
            nota1 = s.nextInt();
            System.out.println("Digite a nota do aluno 2 " + i);
            nota2 = s.nextInt();

            int media = (nota1 + nota2) / 2;

            if (media >= 7) {
                System.out.println("Aluno obteve média " + media + " e está aprovado");
            } else if (media < 7 && media > 3) {
                int notaFinal;
                System.out.println("Digite a nota da final");
                notaFinal = s.nextInt();

                media = (media + notaFinal) / 2;

                if (media >= 5 ){
                    System.out.println("Aluno obteve média " + media + " e está aprovado");
                } else {
                    System.out.println("Aluno obteve média " + media + " e está reprovado");
                }

            } else {
                System.out.println("Aluno obteve média " + media + " e está reprovado");
            }
        }

    }
}
